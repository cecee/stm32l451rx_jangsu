/*
 * hal_spi.c
 *
 */

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
#include "spi.h"

#include "cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define HSPI_TRACE(...)
#define HSPI_TRACE(...)				DBG(__VA_ARGS__)

#define MX_SPI_TIMEOUT		1000	//spi tx/rx timeout
#define SFLASH_SPI_TX_TIMEOUT	1		//serial flash spi tx timeout
#define SFLASH_SPI_RX_TIMEOUT	2		//serial flash spi rx timeout

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
//extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/
int Hal_Spi1_Init(void);
int Hal_Spi2_Init(void);

/*****************************************************************************
*                                Functions
******************************************************************************/
int Hal_Spi_Init(void)
{
	Hal_Spi1_Init();
//	Hal_Spi2_Init();
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Spi1_Init()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi1_Init(void)
{
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Spi2_Init()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi2_Init(void)
{
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Spi2_CS()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi1_CS(unsigned char setval)
{
	if(setval==SET_ON)HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_RESET);
	else HAL_GPIO_WritePin(GPIOB, GPIO_PIN_6, GPIO_PIN_SET);
	return RET_OK;
}

#if 0
/*****************************************************************************
*  Name:        Hal_Spi1_Error()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi1_Error(void)
{
	/* De-initialize the SPI communication BUS */
	DBG(ERR,"Hal_Spi1_Error\n");
	if(HAL_SPI_DeInit(&hspi1)!=HAL_OK)return RET_ERR;

	/* Re- Initiaize the SPI communication BUS */
	MX_SPI1_Init();

	return RET_OK;
}

#endif 
/*****************************************************************************
*  Name:        Hal_Spi2_Error()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi2_Error(void)
{
	/* De-initialize the SPI communication BUS */
	if(HAL_SPI_DeInit(&hspi2)!=HAL_OK)return RET_ERR;

	/* Re- Initiaize the SPI communication BUS */
	MX_SPI2_Init();

	return RET_OK;
}

#if 0
/*****************************************************************************
*  Name:        Hal_Spi1_Read()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Spi1_Read(unsigned char *rdata, unsigned short rlen)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	status = HAL_SPI_Receive(&hspi1, rdata, rlen, SFLASH_SPI_RX_TIMEOUT); 
	/* Check the communication status */
	if(status != HAL_OK)
	{
	  /* Execute user timeout callback */
		DBG(ERR,"Hal_Spi1_Read error!!\n");
	  Hal_Spi1_Error();
	}
	
	return RET_OK;
}


/*****************************************************************************
*  Name:        Hal_Spi1_Write()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Spi1_Write(unsigned char *wdata, unsigned short wlen)
{
	HAL_StatusTypeDef status = HAL_OK;
	
	status = HAL_SPI_Transmit(&hspi1, wdata, wlen, SFLASH_SPI_TX_TIMEOUT);
	
	/* Check the communication status */
	if(status != HAL_OK)
	{
	  /* Execute user timeout callback */
	  Hal_Spi1_Error();

	  return RET_ERR;
	}

	return RET_OK;
}


/*****************************************************************************
*  Name:        Hal_Spi1_WriteRead()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Spi1_WriteRead(unsigned char wdata, unsigned char *rdata, unsigned short rlen)
{
	return RET_OK;
}
#endif 

/*****************************************************************************
*  Name:        Hal_Spi2_Write_()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi2_Write(unsigned int wdata)
{
	HAL_StatusTypeDef status = HAL_OK;
	uint8_t data[2];
	__disable_irq();
	data[1]=wdata&0xff;
	data[0]=(wdata>>8)&0xff;
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	//__HAL_LOCK(hspi); HAL_SPI_Transmit함수에서 조치했음
 	HAL_SPI_Init(&hspi2);	
	__HAL_UNLOCK(&hspi2);
#if 1
	status = HAL_SPI_Transmit(&hspi2, (uint8_t *)data, 2, 1);
	if(status != HAL_OK)
	{
	  /* Execute user timeout callback */
	  Hal_Spi2_Error();
#ifdef LOGOUT		  
	  printf("spi error !!\r\n");
#endif
	  __enable_irq();
	  HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	  return RET_ERR;
	}
#endif	
	/* Check the communication status */
	//while(HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY);
     HAL_SPI_DeInit(&hspi2);	
  	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	__enable_irq();
	return RET_OK;
}

int Hal_Spi2_WriteData(unsigned char* c, int length)
{
	HAL_StatusTypeDef status = HAL_OK;
	__disable_irq();
	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_RESET);
	//__HAL_LOCK(hspi); HAL_SPI_Transmit함수에서 조치했음
 	HAL_SPI_Init(&hspi2);	
	__HAL_UNLOCK(&hspi2);
#if 1
	status = HAL_SPI_Transmit(&hspi2, c, length, 10);
	if(status != HAL_OK)
	{
	  /* Execute user timeout callback */
	  Hal_Spi2_Error();
#ifdef LOGOUT		  
	  printf("spi error !!\r\n");
#endif
	  __enable_irq();
	  HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	  return RET_ERR;
	}
#endif	
	/* Check the communication status */
	//while(HAL_SPI_GetState(&hspi2) != HAL_SPI_STATE_READY);
     HAL_SPI_DeInit(&hspi2);	
  	HAL_GPIO_WritePin(CS_GPIO_Port, CS_Pin, GPIO_PIN_SET);
	__enable_irq();
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Spi1_WriteRead()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Spi2_WriteRead(unsigned char wdata, unsigned char *rdata, unsigned short rlen)
{
	return RET_OK;
}
