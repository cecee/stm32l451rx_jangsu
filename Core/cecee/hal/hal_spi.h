/*
 * hal_spi.h
 *
 */ 
#ifndef HAL_SPI_H_
#define HAL_SPI_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

/****************************************************************************
*                                Functions
*****************************************************************************/
int Hal_Spi_Init(void);
int Hal_Spi1_CS(unsigned char setval);
int Hal_Spi1_Read(unsigned char *rdata, unsigned short rlen);
int Hal_Spi1_Write(unsigned char *wdata, unsigned short wlen);
int Hal_Spi1_WriteRead(unsigned char wdata, unsigned char *rdata, unsigned short rlen);

int Hal_Spi2_Init(void);
int Hal_Spi2_Write(unsigned int wdata);
int Hal_Spi2_WriteData(unsigned char* c, int length);
int Hal_Spi2_WriteRead(unsigned char wdata, unsigned char *rdata, unsigned short rlen);

#endif /* HAL_SPI_H_ */
