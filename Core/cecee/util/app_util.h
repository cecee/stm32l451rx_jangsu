/*
 * app_util.h
 *
 */


#ifndef APP_UTIL_H_
#define APP_UTIL_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

/****************************************************************************
*                                Functions
*****************************************************************************/
//void HAL_Delay(uint32_t Delay);
char* Util_FindString(char *line, unsigned short len,char *str);
char* Util_FindLine(char *line, unsigned short len,char *str);
int Util_FindNumberStrStr(char *str, char *strStart, char *strEnd);

double Util_Stod(const char* s);
double Util_Trunc(double d);
int Util_Pow(int a, int b);

unsigned short Util_CvtEndianShort(unsigned short param);
unsigned int Util_CvtEndianInt(unsigned int param);
char * Util_Strtok (char *str, char *seps);
int Util_Stoi(const char* s);
int Util_BinArrayCmp(unsigned char* a, unsigned char* b);
void Util_Hex_Dump(const char *prt,void *data, int size);

#endif /* APP_UTIL_H_ */
