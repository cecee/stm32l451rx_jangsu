//-----------------------------------
//	ILI9488 Touchscreen library for STM32
//-----------------------------------
//		Touchpad GPIO defines
//		PA0 XH
//		PA1 YH
//		PA2 XL (ADC0)
//		PA3 YL (ADC1)

#include "ILI9488_Touchscreen.h"
#include "stm32l4xx_hal.h"
#include "main.h"
#include "../inc/cecee.h"

const touch_offset_t tOffSet={TP_XA, TP_XB, TP_YA, TP_YB, TP_DX, TP_DY};
touch_pos_t gTpos;

void touch_init(void)
{

  	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.Pin = GPIO_PIN_2;//XL open
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );
	
 	GPIO_InitStructure.Pin = YH_Pin;//YH open
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);	
	GPIO_InitStructure.Pin = GPIO_PIN_3;//YL gnd
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure ); 

	GPIO_InitStructure.Pin = XH_Pin;//GPIO_MODE_IT_FALLING
	GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(XH_GPIO_Port, &GPIO_InitStructure);
}


uint16_t adcRead(uint8_t adc)
{
  uint16_t value;
  ADC_ChannelConfTypeDef sConfig;
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
/*
  sConfig.Channel = ADC_CHANNEL_2;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  } 
 */ 
  
  /**Configure Regular Channel */
  if(adc==0)sConfig.Channel = ADC_CHANNEL_7;
  else sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  ////sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  /*if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }*/
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  } 
   
  HAL_ADC_Start (&hadc1); 
  HAL_ADC_PollForConversion (&hadc1, HAL_MAX_DELAY); 
  value = HAL_ADC_GetValue (&hadc1); //XL
  //printf("adcRead [%d]\r\n",value);
  return value;
}


//Internal Touchpad command, do not call directly
uint16_t TouchReadX(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.Pin = XH_Pin;//XH open
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
	
	HAL_GPIO_WritePin(GPIOA, YH_Pin, GPIO_PIN_SET);
	GPIO_InitStructure.Pin = YH_Pin;//YH vcc
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
  
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);	
	GPIO_InitStructure.Pin = GPIO_PIN_3;//YL gnd
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
	
	GPIO_InitStructure.Pin = GPIO_PIN_2;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
    //XL ADC
	return adcRead(0);
}


uint16_t TouchReadY(void)
{
	uint16_t value = 0;
	GPIO_InitTypeDef GPIO_InitStructure;
	
	HAL_GPIO_WritePin(GPIOA, XH_Pin, GPIO_PIN_SET);
	GPIO_InitStructure.Pin = XH_Pin;//XH vcc
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
	
	GPIO_InitStructure.Pin = YH_Pin;//YH open
	GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
  
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_2, GPIO_PIN_RESET);
	GPIO_InitStructure.Pin = GPIO_PIN_2;//XL gnd
	GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init( GPIOA , &GPIO_InitStructure );  
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_3, GPIO_PIN_RESET);	
	
	GPIO_InitStructure.Pin = GPIO_PIN_3;
	GPIO_InitStructure.Mode = GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	//YL ADC   
	value = adcRead(1);
	//printf("TouchReadY[%d]\r\n",value);
    return value;
}

void TouchRead(void)
{
  
  uint16_t x,y;
  uint16_t xPos,yPos;
  x = TouchReadX();
  y = TouchReadY();
  touch_init();
  if(x<tOffSet.xA || x>tOffSet.xB) return;
  if(y<tOffSet.yA || y>tOffSet.yB) return;

  xPos= (480*(x-tOffSet.xA))/tOffSet.deltaX;
  yPos= (320*(y-tOffSet.yA))/tOffSet.deltaY;
  gTpos.x= xPos;
  gTpos.y= yPos;
  return;
}
#if 0
//Internal Touchpad command, do not call directly
void TP_Write(uint8_t value)
{
    uint8_t i = 0x08;

		HAL_GPIO_WritePin(TP_CLK_PORT, TP_CLK_PIN, GPIO_PIN_RESET);	
	
    while(i > 0)
    {
        if((value & 0x80) != 0x00)
        {
	  HAL_GPIO_WritePin(TP_MOSI_PORT, TP_MOSI_PIN, GPIO_PIN_SET);
        }
        else
        {
	 HAL_GPIO_WritePin(TP_MOSI_PORT, TP_MOSI_PIN, GPIO_PIN_RESET);
        }

        value <<= 1;
				HAL_GPIO_WritePin(TP_CLK_PORT, TP_CLK_PIN, GPIO_PIN_SET);
				HAL_GPIO_WritePin(TP_CLK_PORT, TP_CLK_PIN, GPIO_PIN_RESET);        
        i--;
    };
}



//Read coordinates of touchscreen press. Position[0] = X, Position[1] = Y
uint8_t TP_Read_Coordinates(uint16_t Coordinates[2])
{
		HAL_GPIO_WritePin(TP_CLK_PORT, TP_CLK_PIN, GPIO_PIN_SET);		
		HAL_GPIO_WritePin(TP_MOSI_PORT, TP_MOSI_PIN, GPIO_PIN_SET);		
		HAL_GPIO_WritePin(TP_CS_PORT, TP_CS_PIN, GPIO_PIN_SET);		

	
	
    uint32_t avg_x, avg_y = 0;		
		uint16_t rawx, rawy = 0;	
		uint32_t calculating_x, calculating_y = 0;
	
    uint32_t samples = NO_OF_POSITION_SAMPLES;
    uint32_t counted_samples = 0;

		HAL_GPIO_WritePin(TP_CS_PORT, TP_CS_PIN, GPIO_PIN_RESET);

	
    while((samples > 0)&&(HAL_GPIO_ReadPin(TP_IRQ_PORT, TP_IRQ_PIN) == 0))
    {			
        TP_Write(CMD_RDY);

				rawy = TP_Read();	
				avg_y += rawy;
				calculating_y += rawy;

				
        TP_Write(CMD_RDX);
        rawx = TP_Read();
				avg_x += rawx;
				calculating_x += rawx;
        samples--;
				counted_samples++;
    };
		
		HAL_GPIO_WritePin(TP_CS_PORT, TP_CS_PIN, GPIO_PIN_SET);

		
		if((counted_samples == NO_OF_POSITION_SAMPLES)&&(HAL_GPIO_ReadPin(TP_IRQ_PORT, TP_IRQ_PIN) == 0))
		{
		
		calculating_x /= counted_samples;
		calculating_y /= counted_samples;
		
		rawx = calculating_x;
		rawy = calculating_y;		
		
		rawx *= -1;
		rawy *= -1;
		
		//CONVERTING 16bit Value to Screen coordinates
    // 65535/273 = 240!
		// 65535/204 = 320!
    Coordinates[0] = ((240 - (rawx/X_TRANSLATION)) - X_OFFSET)*X_MAGNITUDE;
		Coordinates[1] = ((rawy/Y_TRANSLATION)- Y_OFFSET)*Y_MAGNITUDE;
		
		return TOUCHPAD_DATA_OK;			
		}
		else
		{
			Coordinates[0] = 0;
			Coordinates[1] = 0;
			return TOUCHPAD_DATA_NOISY;
		}
}

//Check if Touchpad was pressed. Returns TOUCHPAD_PRESSED (1) or TOUCHPAD_NOT_PRESSED (0)
uint8_t TP_Touchpad_Pressed(void)
{
	if(HAL_GPIO_ReadPin(TP_IRQ_PORT, TP_IRQ_PIN) == 0)
	{
		return TOUCHPAD_PRESSED;
	}
	else
	{
		return TOUCHPAD_NOT_PRESSED;
	}
}

#endif
