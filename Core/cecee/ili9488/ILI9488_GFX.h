//-----------------------------------
//	ILI9488 GFX library for STM32
//-----------------------------------
#ifndef ILI9341_GFX_H
#define ILI9341_GFX_H

#include "stm32l4xx_hal.h"
#include "ILI9488_STM32_Driver.h"

#define HORIZONTAL_IMAGE	0
#define VERTICAL_IMAGE	1

void ILI9341_Draw_Hollow_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
void ILI9341_Draw_Filled_Circle(uint16_t X, uint16_t Y, uint16_t Radius, color_t color);
void ILI9341_Draw_Hollow_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
void ILI9341_Draw_Filled_Rectangle_Coord(uint16_t X0, uint16_t Y0, uint16_t X1, uint16_t Y1, color_t color);
void ILI9341_Draw_Char(char Character, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
void ILI9341_Draw_Text(const char* Text, uint16_t X, uint16_t Y, color_t color, uint16_t Size, color_t Background_Colour);
void ILI9341_Draw_Filled_Rectangle_Size_Text(uint16_t X0, uint16_t Y0, uint16_t Size_X, uint16_t Size_Y, color_t color);

void ILI9341_Draw_LedBar(uint16_t x, uint16_t y, uint16_t value);

//USING CONVERTER: http://www.digole.com/tools/PicturetoC_Hex_converter.php
//65K colour (2Bytes / Pixel)
//void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation);
void ILI9341_Draw_Image(const char* Image_Array, uint8_t Orientation, uint16_t posX, uint16_t posY, uint16_t imgX, uint16_t imgY);
#endif
