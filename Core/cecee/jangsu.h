#ifndef JANGSU_H
#define JANGSU_H
#include "stm32l4xx_hal.h"

typedef struct {
	uint8_t jajang;
	uint8_t lock;
	uint8_t safe;
	uint8_t outgoing;
	uint8_t power;
	uint8_t currentOndo;
	uint8_t setOndo;
} BOX_VALUE;

extern BOX_VALUE gBoxVal;

void jangsuInitPage(void);
#endif